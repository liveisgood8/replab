﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_dll1
{
    public class Weapon
    {
        protected int _price;
        protected double _weight;
        protected string _model;


        public int Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public virtual double Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }



    }

    public class Gun : Weapon
    {
        protected int _capacity;
        protected int _clips;
        protected bool _type;

        public int Capacity
        {
            get { return _capacity; }
            set { _capacity = value; }
        }

        public int Clips
        {
            get { return _clips; }
            set { _clips = value; }
        }

        public bool Type
        {
            get { return _type; }
            set {
                _type = value;
                if (_type == true) _capacity = 9; else _capacity = 30;
            }
        }

        public override double Weight
        {
            get { return _weight; }
            set
            {
                _weight = value + _clips * _capacity * 0.005;
            }
        }
        public Gun(string model, int price, int clips, double weight,  bool type)
        {
            Model = model;
            Type = type;
            Price = price;
            Clips = clips;
            Weight = weight;
        }
    }

    public class Blade : Weapon
    {
        protected int _handcount;

        public int HandCount
        {
            get { return _handcount; }
            set { _handcount = value; }
        }

        public Blade(string model, int price, double weight, int handcount)
        {
            Model = model;
            Price = price;
            Weight = weight;
            HandCount = handcount;
        }
    }
}
