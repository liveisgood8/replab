﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_dll1
{
    public class WeaponFactory
    {
        private static string[] pistol = {"PM", "Deseart Eagle", "Glock" };
        private static string[] automatic = { "AK47", "AK74", "M4A4", "M4A1", "UZI" };
        private static string[] blade = {"Blade 1", "Blade 2", "Blade 3" };
        
        public static Division CreateDivision()
        {
            Random rand = new Random();
            Division division = new Division();
            int type = 0;
            for (int i=0;i<rand.Next(10,100); i++)
            {
                type = rand.Next(0, 3);
                if (type == 0)
                    division.AddWeapon(new Gun(automatic[rand.Next(0, automatic.Length)], rand.Next(30, 300), rand.Next(1, 10), rand.NextDouble() * ((10.0 - 5.0) + 5.0), false));
                else if(type ==1)
                    division.AddWeapon(new Gun(pistol[rand.Next(0, pistol.Length)], rand.Next(10, 200), rand.Next(1, 10), rand.NextDouble() * ((5.0 - 2.5) + 2.5), true));
                else
                    division.AddWeapon(new Blade(blade[rand.Next(0, blade.Length)], rand.Next(10, 100),rand.NextDouble() * ((2.0 - 0.5) + 0.5), rand.Next(1,2)));
            }
            return division;
        }
    }
}
