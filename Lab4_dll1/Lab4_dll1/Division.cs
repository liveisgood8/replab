﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab4_dll1
{
   public  class Division
    {
        private List<Weapon> weapons = new List<Weapon>();

        public void AddWeapon(Weapon weapon) {
            weapons.Add(weapon);
        }

        public void RemoveWeapon(Weapon weapon)
        {
            weapons.Remove(weapon);
        }

        public double GetTotalWeight()
        {
            double totalweight = 0;
            foreach (Weapon weapon in weapons)
            {
                totalweight += weapon.Weight;
            }
            return Math.Round(totalweight, 3);
        }
        public List<Weapon> GetWeaponList()
        {
            return weapons;
        }
        public void Print()
        {
            List<Weapon> weapons = GetWeaponList();
            for (int i = 0; i < weapons.Count; i++)
            {
                Console.WriteLine((i + 1) + ". " + weapons[i].Model + ", " +  Math.Round(weapons[i].Weight, 3).ToString() + "kg");
            }
            Console.WriteLine("Total weight = " + GetTotalWeight() + "kg");
        }
    }

    
}
