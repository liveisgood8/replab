﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab4_dll1;

namespace lab4
{
    public partial class AddWeapon : Form
    {
        private Weapon _weapon;
        private DialogResult _result;

        public Weapon Weapon
        {
            get { return _weapon; }
        }
        public DialogResult Result
        {
            get { return _result; }
        }

        public AddWeapon()
        {
            InitializeComponent();

        }

        public AddWeapon(Weapon weapon) : this()
        {
            if (weapon != null)
            {
                WeaponClass.Text = weapon.GetType().ToString();
                modelText.Text = weapon.Model.ToString();
                priceText.Text = weapon.Price.ToString();
                weightText.Text = weapon.Weight.ToString();

                if (weapon.GetType() == typeof(Gun))
                {
                    Gun _weapon = (Gun)weapon;
                    clipsText.Text = _weapon.Clips.ToString();
                }
                else if (weapon.GetType() == typeof(Blade))
                {
                    Blade _weapon = (Blade)weapon;
                    hcText.Text = _weapon.HandCount.ToString();
                }
            }
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            if (WeaponClass.Text == "Gun")
            {
                if (TypeComBox.Text == "Automatic")
                {
                    _weapon = new Gun(modelText.Text, Convert.ToInt32(priceText.Text), Convert.ToInt32(clipsText.Text), Convert.ToDouble(weightText.Text), false);
                }
                else if(TypeComBox.Text == "Pistol")
                {
                    _weapon = new Gun(modelText.Text, Convert.ToInt32(priceText.Text), Convert.ToInt32(clipsText.Text), Convert.ToDouble(weightText.Text), true);
                }
            }
            else
            {
                _weapon = new Blade(modelText.Text, Convert.ToInt32(priceText.Text), Convert.ToDouble(weightText.Text), Convert.ToInt32(hcText.Text));
            }
            _result = DialogResult.OK;
            this.Close();
        }

        private void WeaponClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(WeaponClass.Text == "Gun")
            {
                hcText.Enabled = false;
                label7.Enabled = false;
                clipsText.Enabled = true;
                label6.Enabled = true;
                TypeComBox.Enabled = true;
                label2.Enabled = true;
            }
            else if(WeaponClass.Text == "Blade")
            {
                hcText.Enabled = true;
                label7.Enabled = true;
                clipsText.Enabled = false;
                label6.Enabled = false;
                TypeComBox.Enabled = false;
                label2.Enabled = false;
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            _result = DialogResult.Cancel;
            this.Close();
        }
    }
}
