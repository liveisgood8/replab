﻿namespace lab4
{
    partial class AddWeapon
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.WeaponClass = new System.Windows.Forms.ComboBox();
            this.TypeComBox = new System.Windows.Forms.ComboBox();
            this.modelText = new System.Windows.Forms.TextBox();
            this.priceText = new System.Windows.Forms.TextBox();
            this.weightText = new System.Windows.Forms.TextBox();
            this.clipsText = new System.Windows.Forms.TextBox();
            this.hcText = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(44, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Class";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 43);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Type";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(40, 76);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(36, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Model";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(42, 116);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 3;
            this.label4.Text = "Price";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(35, 151);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 4;
            this.label5.Text = "Weight";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(44, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(29, 13);
            this.label6.TabIndex = 5;
            this.label6.Text = "Clips";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(15, 219);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(61, 13);
            this.label7.TabIndex = 6;
            this.label7.Text = "HandCount";
            // 
            // WeaponClass
            // 
            this.WeaponClass.FormattingEnabled = true;
            this.WeaponClass.Items.AddRange(new object[] {
            "Gun",
            "Blade"});
            this.WeaponClass.Location = new System.Drawing.Point(82, 12);
            this.WeaponClass.Name = "WeaponClass";
            this.WeaponClass.Size = new System.Drawing.Size(121, 21);
            this.WeaponClass.TabIndex = 7;
            this.WeaponClass.SelectedIndexChanged += new System.EventHandler(this.WeaponClass_SelectedIndexChanged);
            // 
            // TypeComBox
            // 
            this.TypeComBox.FormattingEnabled = true;
            this.TypeComBox.Items.AddRange(new object[] {
            "Automatic",
            "Pistol"});
            this.TypeComBox.Location = new System.Drawing.Point(82, 40);
            this.TypeComBox.Name = "TypeComBox";
            this.TypeComBox.Size = new System.Drawing.Size(121, 21);
            this.TypeComBox.TabIndex = 8;
            // 
            // modelText
            // 
            this.modelText.Location = new System.Drawing.Point(82, 76);
            this.modelText.Name = "modelText";
            this.modelText.Size = new System.Drawing.Size(121, 20);
            this.modelText.TabIndex = 9;
            // 
            // priceText
            // 
            this.priceText.Location = new System.Drawing.Point(82, 113);
            this.priceText.Name = "priceText";
            this.priceText.Size = new System.Drawing.Size(121, 20);
            this.priceText.TabIndex = 10;
            // 
            // weightText
            // 
            this.weightText.Location = new System.Drawing.Point(82, 151);
            this.weightText.Name = "weightText";
            this.weightText.Size = new System.Drawing.Size(121, 20);
            this.weightText.TabIndex = 11;
            // 
            // clipsText
            // 
            this.clipsText.Location = new System.Drawing.Point(82, 184);
            this.clipsText.Name = "clipsText";
            this.clipsText.Size = new System.Drawing.Size(121, 20);
            this.clipsText.TabIndex = 12;
            // 
            // hcText
            // 
            this.hcText.Location = new System.Drawing.Point(82, 219);
            this.hcText.Name = "hcText";
            this.hcText.Size = new System.Drawing.Size(121, 20);
            this.hcText.TabIndex = 13;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(18, 263);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 14;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(128, 263);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 15;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // AddWeapon
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(234, 298);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.hcText);
            this.Controls.Add(this.clipsText);
            this.Controls.Add(this.weightText);
            this.Controls.Add(this.priceText);
            this.Controls.Add(this.modelText);
            this.Controls.Add(this.TypeComBox);
            this.Controls.Add(this.WeaponClass);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "AddWeapon";
            this.Text = "AddWeapon";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox WeaponClass;
        private System.Windows.Forms.ComboBox TypeComBox;
        private System.Windows.Forms.TextBox modelText;
        private System.Windows.Forms.TextBox priceText;
        private System.Windows.Forms.TextBox weightText;
        private System.Windows.Forms.TextBox clipsText;
        private System.Windows.Forms.TextBox hcText;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
    }
}