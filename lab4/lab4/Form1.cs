﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Lab4_dll1;

namespace lab4
{
    public partial class Form1 : Form
    {
        private Division division = new Division();
        public Form1()
        {
            InitializeComponent();
        }

        public void RefreshWeight()
        {
            label1.Text = Math.Round(division.GetTotalWeight(),3).ToString() + "kg";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AddWeapon f2 = new AddWeapon();
            f2.ShowDialog();
            if(f2.Result == DialogResult.OK)
            {
                weaponList.Items.Add(f2.Weapon.Model + "-" + Math.Round(f2.Weapon.Weight,3).ToString() + "kg");
                division.AddWeapon(f2.Weapon);
                RefreshWeight();
            }
            f2.Dispose();
            
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if(weaponList.SelectedItem != null)
            {
                AddWeapon f2 = new AddWeapon(division.GetWeaponList()[weaponList.SelectedIndex]);
                f2.ShowDialog();
                if(f2.Result == DialogResult.OK)
                {
                    division.GetWeaponList()[weaponList.SelectedIndex] = f2.Weapon;
                    RefreshWeight();
                }
                f2.Dispose();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if(weaponList.SelectedItem != null)
            {
                division.RemoveWeapon(division.GetWeaponList()[weaponList.SelectedIndex]);
                weaponList.Items.RemoveAt(weaponList.SelectedIndex);
                RefreshWeight();
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            division = WeaponFactory.CreateDivision();
            List<Weapon> weapons = division.GetWeaponList();

            weaponList.Items.Clear();
            foreach(Weapon weapon in weapons)
            {
                weaponList.Items.Add(weapon.Model + "-" + Math.Round(weapon.Weight,3).ToString() + "kg");

            }
            RefreshWeight();
        }
    }
}
